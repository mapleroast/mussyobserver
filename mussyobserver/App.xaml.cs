﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using Windows.Graphics.Imaging;
using Windows.Media.Ocr;
using Windows.Storage;
using Windows.Storage.Streams;
using Point = System.Drawing.Point;
using System.Speech;
using System.Speech.Synthesis;
using Fastenshtein;

namespace mussyobserver
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        //####################################################################################################
        //▼▼定数▼▼
        //####################################################################################################

        //共通
        private const string DEFAULT_WINDOWTITLE = "MapleStory";
        private const int    DEFAULT_CHAT_LEFTTOP_X = 0,        //チャット欄左上X座標
                             DEFAULT_CHAT_LEFTTOP_Y = 550,      //チャット欄左上Y座標
                             DEFAULT_CHAT_RIGHTBOTTOM_X = 570,  //チャット欄右下X座標
                             DEFAULT_CHAT_RIGHTBOTTOM_Y = 780,  //チャット欄右下Y座標
                             DEFAULT_MVP_IDLETIME = 5;          //MVP通知スパン(分)

        private const double TEXT_MATCH_RATIO = 0.85;           //テキスト類似度しきい値

        //マクロ探知機検出用
        //(デフォルト値、設定ファイル読込失敗時に適用される)
        private static string[] DEFAULT_KEYWORDS =
        {   "マクロ","探知","探知機","知機","発動",
            "入力のた","力のため","回クリック",
            "以下の","画像に","表示された","示された",
            "通りに","エンターキーを","押して"
        };

        //MVP検出用
        //(デフォルト値、設定ファイル読込失敗時に適用される)
        private static string[] DEFAULT_KEYWORDS_MVP =
        {
            "mvp","MVP","ＭＶＰ","EXP","ＥＸＰ","jkm","ｊｋｍ","マイスタ","ﾏｲｽﾀ"
        };

        //除外キーワード
        //(デフォルト値、設定ファイル読込失敗時に適用される)
        private static string[] DEFAULT_KEYWORDS_IGNORE =
{
            "ルーン","矢印"
        };

        //設定読み込み用
        private static string _iniFileName = AppDomain.CurrentDomain.BaseDirectory + "settings.ini";
        private static string _csvFileName_keywords = AppDomain.CurrentDomain.BaseDirectory + "keywords.csv";
        private static string _csvFilename_ignore = AppDomain.CurrentDomain.BaseDirectory + "ignore.csv";
        private static string _csvFilename_mvp = AppDomain.CurrentDomain.BaseDirectory + "mvp.csv";


        //####################################################################################################
        //▼▼変数(宣言のみ)▼▼
        //####################################################################################################
        private static List<string> _keywords, _keywords_ignore, _keywords_mvp;
        private static string _targetWindowTitle;
        private static string _prev_mvp;
        private static bool _isDebug, _isSplit, _isGrayScale, _isMVP;
        private static int _chatWindowLTx, _chatWindowLTy, _chatWindowRBx, _chatWindowRBy,_mvp_idleTime;
        private static DateTime _timeCache;
        private static System.Windows.Forms.NotifyIcon _notifyIcon;
        private static Timer _timer;
        private static SoundPlayer _sound;
        private static SpeechSynthesizer _speech;

        // ini読み込みの前準備
        [DllImport("KERNEL32.DLL")]
        public static extern uint GetPrivateProfileString
            (
                string lpAppName,
                string lpKeyName,
                string lpDefault,
                StringBuilder lpReturnedString,
                uint nSize,
                string lpFileName
            );

        //ウィンドウ取得の前準備

        //パターン①用
        [DllImport("user32.dll")]
        private static extern bool GetWindowRect(IntPtr hwnd, out RECT lpRect);
        //パターン①、②用
        [System.Runtime.InteropServices.DllImport("User32.dll")]
        private extern static bool PrintWindow(IntPtr hwnd, IntPtr hDC, uint nFlags);

        //ウィンドウキャプチャの前準備
        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }
        [StructLayout(LayoutKind.Sequential)]
        private struct POINT
        {
            public int x;
            public int y;
        }

        [DllImport("user32.dll")]
        private static extern int GetClientRect(IntPtr hwnd, out RECT lpRect);
        [DllImport("user32.dll")]
        private static extern bool ClientToScreen(IntPtr hwnd, out POINT lpPoint);


        //####################################################################################################
        //▼▼メイン処理▼▼
        //####################################################################################################

        //アプリケーション起動時の処理
        protected override void OnStartup(StartupEventArgs e)
        {
            //設定の読み込み
            LoadSettings();

            //タスクトレイのコンテキストメニュー
            var menu = new System.Windows.Forms.ContextMenuStrip();
            menu.Items.Add("終了", null, Exit_Click);

            base.OnStartup(e);

            //タスクトレイアイコンの設定
            var icon = GetResourceStream(new Uri("./Resources/detector_g.ico", UriKind.Relative)).Stream;
            _notifyIcon = new System.Windows.Forms.NotifyIcon
            {
                Visible = true,
                Icon = new System.Drawing.Icon(icon),
                Text = "Mussy Observer",
                ContextMenuStrip = menu
            };
            _notifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(NotifyIcon_Click);

            //アラート音の設定
            _sound = new SoundPlayer(mussyobserver.Properties.Resources.alert);

            //テキスト読み上げの設定
            _speech = new SpeechSynthesizer();
            _speech.SetOutputToDefaultAudioDevice();
            _speech.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult);

            //以前のmvp検出文の初期化
            _prev_mvp = "";
            //以前のmvp検出時間の初期化
            _timeCache = DateTime.MinValue;

            //状態監視を開始(5秒ごと)
            _timer = new Timer(5000);
            _timer.Elapsed += (sender, te) =>
            {
                Observer();
                GC.Collect();
            };
            _timer.Start();

        }


        //####################################################################################################
        //▼▼イベントハンドラ▼▼
        //####################################################################################################

        //タスクトレイアイコンクリック時
        private void NotifyIcon_Click(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                //現状は何も起こらない
            }
        }

        //アプリケーションの終了
        private void Exit_Click(object sender, EventArgs e)
        {
            Shutdown();
        }


        //####################################################################################################
        //▼▼関数▼▼
        //####################################################################################################

        //=============================================================
        //各種設定ファイルの読み込み
        //=============================================================
        private void LoadSettings()
        {
            _keywords = new List<string>();
            _keywords_ignore = new List<string>();
            _keywords_mvp = new List<string>();

            //iniファイルから設定のの読み込み
            StringBuilder sb = new StringBuilder(1024);
            try
            {

                //ウィンドウタイトル
                GetPrivateProfileString(
                    "APPLICATION",      // セクション名
                    "WindowTitle",          // キー名    
                    DEFAULT_WINDOWTITLE,   // 値が取得できなかった場合に返される初期値
                    sb,             // 格納先
                    Convert.ToUInt32(sb.Capacity), // 格納先のキャパ
                    _iniFileName);   // iniファイル名
                _targetWindowTitle = sb.ToString();
                Console.WriteLine(_targetWindowTitle);

                sb = new StringBuilder(1024);
                //デバッグモードの有効化
                GetPrivateProfileString(
                    "APPLICATION",      // セクション名
                    "DebugMode",          // キー名    
                    "false",   // 値が取得できなかった場合に返される初期値
                    sb,             // 格納先
                    Convert.ToUInt32(sb.Capacity), // 格納先のキャパ
                    _iniFileName);   // iniファイル名
                switch (sb.ToString())
                {
                    case "true": _isDebug = true; break;
                    case "false": _isDebug = false; break;
                    default: _isDebug = false; break;
                }

                sb = new StringBuilder(1024);
                //分割スキャンモードの有効化
                GetPrivateProfileString(
                    "OCRSCAN",      // セクション名
                    "SplitMode",          // キー名    
                    "false",   // 値が取得できなかった場合に返される初期値
                    sb,             // 格納先
                    Convert.ToUInt32(sb.Capacity), // 格納先のキャパ
                    _iniFileName);   // iniファイル名
                switch (sb.ToString())
                {
                    case "true": _isSplit = true; break;
                    case "false": _isSplit = false; break;
                    default: _isSplit = false; break;
                }

                sb = new StringBuilder(1024);
                //グレースケールモードの有効化
                GetPrivateProfileString(
                    "OCRSCAN",      // セクション名
                    "GrayScaleMode",          // キー名    
                    "false",   // 値が取得できなかった場合に返される初期値
                    sb,             // 格納先
                    Convert.ToUInt32(sb.Capacity), // 格納先のキャパ
                    _iniFileName);   // iniファイル名
                switch (sb.ToString())
                {
                    case "true": _isGrayScale = true; break;
                    case "false": _isGrayScale = false; break;
                    default: _isGrayScale = false; break;
                }
                switch (sb.ToString())
                {
                    case "true": _isGrayScale = true; break;
                    case "false": _isGrayScale = false; break;
                    default: _isGrayScale = false; break;
                }

                sb = new StringBuilder(1024);
                //MVP検出モードの有効化
                GetPrivateProfileString(
                    "MVP",      // セクション名
                    "MVPMode",          // キー名    
                    "true",   // 値が取得できなかった場合に返される初期値
                    sb,             // 格納先
                    Convert.ToUInt32(sb.Capacity), // 格納先のキャパ
                    _iniFileName);   // iniファイル名
                switch (sb.ToString())
                {
                    case "true": _isMVP = true; break;
                    case "false": _isMVP = false; break;
                    default: _isMVP = false; break;
                }

                //チャット欄範囲の取得
                sb = new StringBuilder(1024);
                //左上X座標
                GetPrivateProfileString(
                   "MVP",      // セクション名
                    "ChatWindowLeftTopX",          // キー名    
                    DEFAULT_CHAT_LEFTTOP_X.ToString(),   // 値が取得できなかった場合に返される初期値
                    sb,             // 格納先
                    Convert.ToUInt32(sb.Capacity), // 格納先のキャパ
                    _iniFileName);   // iniファイル名
                try
                {
                    _chatWindowLTx = int.Parse(sb.ToString());
                }
                catch(Exception e)
                {
                    _chatWindowLTx = DEFAULT_CHAT_LEFTTOP_X;
                }

                sb = new StringBuilder(1024);
                //左上Y座標
                GetPrivateProfileString(
                   "MVP",      // セクション名
                    "ChatWindowLeftTopY",          // キー名    
                    DEFAULT_CHAT_LEFTTOP_Y.ToString(),   // 値が取得できなかった場合に返される初期値
                    sb,             // 格納先
                    Convert.ToUInt32(sb.Capacity), // 格納先のキャパ
                    _iniFileName);   // iniファイル名
                try
                {
                    _chatWindowLTy = int.Parse(sb.ToString());
                }
                catch (Exception e)
                {
                    _chatWindowLTy = DEFAULT_CHAT_LEFTTOP_Y;
                }

                sb = new StringBuilder(1024);
                //右下X座標
                GetPrivateProfileString(
                   "MVP",      // セクション名
                    "ChatWindowRightBottomX",          // キー名    
                    DEFAULT_CHAT_RIGHTBOTTOM_X.ToString(),   // 値が取得できなかった場合に返される初期値
                    sb,             // 格納先
                    Convert.ToUInt32(sb.Capacity), // 格納先のキャパ
                    _iniFileName);   // iniファイル名
                try
                {
                    _chatWindowRBx = int.Parse(sb.ToString());
                }
                catch (Exception e)
                {
                    _chatWindowRBx = DEFAULT_CHAT_RIGHTBOTTOM_X;
                }

                sb = new StringBuilder(1024);
                //右下Y座標
                GetPrivateProfileString(
                   "MVP",      // セクション名
                    "ChatWindowRightBottomY",          // キー名    
                    DEFAULT_CHAT_RIGHTBOTTOM_Y.ToString(),   // 値が取得できなかった場合に返される初期値
                    sb,             // 格納先
                    Convert.ToUInt32(sb.Capacity), // 格納先のキャパ
                    _iniFileName);   // iniファイル名
                try
                {
                    _chatWindowRBy = int.Parse(sb.ToString());
                }
                catch (Exception e)
                {
                    _chatWindowRBy = DEFAULT_CHAT_RIGHTBOTTOM_Y;
                }

                sb = new StringBuilder(1024);
                //MVP通知スパン
                GetPrivateProfileString(
                   "MVP",      // セクション名
                    "NotificationIdleTime",          // キー名    
                    DEFAULT_MVP_IDLETIME.ToString(),   // 値が取得できなかった場合に返される初期値
                    sb,             // 格納先
                    Convert.ToUInt32(sb.Capacity), // 格納先のキャパ
                    _iniFileName);   // iniファイル名
                try
                {
                    _mvp_idleTime = int.Parse(sb.ToString());
                }
                catch (Exception e)
                {
                    _mvp_idleTime = DEFAULT_MVP_IDLETIME;
                }

                //=====デバッグ用=====
                if (_isDebug)
                {
                    if (!Directory.Exists(Directory.GetCurrentDirectory() + @"\Debug"))
                        Directory.CreateDirectory("Debug");
                }
                //=====デバッグ用=====
            }
            catch (Exception e)
            {
                _targetWindowTitle = DEFAULT_WINDOWTITLE;
            }

            //検知対象キーワード
            StreamReader sr = new StreamReader(_csvFileName_keywords);

            // csvファイルから検知対象、除外対象のキーワードを読み込み
            try
            {
                {
                    // 末尾まで繰り返す
                    while (!sr.EndOfStream)
                    {
                        // CSVファイルの一行を読み込む
                        string line = sr.ReadLine();
                        // 読み込んだ一行をカンマ毎に分けて配列に格納する
                        string[] values = line.Split(',');

                        // 配列からリストに格納する
                        _keywords.AddRange(values);

                        if (_keywords.Count < 1)
                            _keywords.AddRange(DEFAULT_KEYWORDS);
                    }
                }
                //除外対象キーワード
                sr = new StreamReader(_csvFilename_ignore);
                {
                    // 末尾まで繰り返す
                    while (!sr.EndOfStream)
                    {
                        // CSVファイルの一行を読み込む
                        string line = sr.ReadLine();
                        // 読み込んだ一行をカンマ毎に分けて配列に格納する
                        string[] values = line.Split(',');

                        // 配列からリストに格納する
                        _keywords_ignore.AddRange(values);

                        if (_keywords_ignore.Count < 1)
                            _keywords_ignore.AddRange(DEFAULT_KEYWORDS_IGNORE);
                    }
                }
                //MVPキーワード
                sr = new StreamReader(_csvFilename_mvp);
                {
                    // 末尾まで繰り返す
                    while (!sr.EndOfStream)
                    {
                        // CSVファイルの一行を読み込む
                        string line = sr.ReadLine();
                        // 読み込んだ一行をカンマ毎に分けて配列に格納する
                        string[] values = line.Split(',');

                        // 配列からリストに格納する
                        _keywords_mvp.AddRange(values);

                        if (_keywords_mvp.Count < 1)
                            _keywords_mvp.AddRange(DEFAULT_KEYWORDS_MVP);
                    }
                }
            }
            catch (Exception e)
            {
                if (_keywords.Count < 1)
                    _keywords.AddRange(DEFAULT_KEYWORDS);
                if (_keywords_ignore.Count < 1)
                    _keywords_ignore.AddRange(DEFAULT_KEYWORDS_IGNORE);
                if (_keywords_mvp.Count < 1)
                    _keywords_mvp.AddRange(DEFAULT_KEYWORDS_MVP);
            }
            finally
            {
                sr.Dispose();
            }
        }

        //=============================================================
        //クライアント監視
        //=============================================================
        private void Observer()
        {
            List<string> ocrText = new List<string>();
            Bitmap captureImage = CaptureTargetWindow();    //指定ウィンドウのキャプチャ

            //▼▼MVP検出処理▼▼
            if (_isMVP)
            {
                if (_chatWindowLTx < 0)
                    _chatWindowLTx = DEFAULT_CHAT_LEFTTOP_X;
                if (_chatWindowLTy < 0)
                    _chatWindowLTy = DEFAULT_CHAT_LEFTTOP_Y;
                if (_chatWindowRBx < 0)
                    _chatWindowRBx = DEFAULT_CHAT_RIGHTBOTTOM_X;
                if (_chatWindowRBy < 0)
                    _chatWindowRBy = DEFAULT_CHAT_RIGHTBOTTOM_Y;

                Bitmap chatWindowImage = TrimCaptureImage(captureImage, _chatWindowLTx, _chatWindowLTy, _chatWindowRBx, _chatWindowRBy);

                //キャプチャした画像をOCR用に変換
                Task<SoftwareBitmap> cvBitmap = GetSoftwareSnapShot(chatWindowImage);
                cvBitmap.Wait();
                SoftwareBitmap ocrImage = cvBitmap.Result;


                //OCRスキャン
                Task<OcrResult> ocrResult = RecognizeText(ocrImage);
                ocrResult.Wait();

                //スキャン結果を行ごとに処理
                foreach (var line in ocrResult.Result.Lines)
                {
                    //スキャンした文字列を変数に追加
                    ocrText.Add(line.Text);
                }

                //=====デバッグ用=====
                if (_isDebug)
                {
                    foreach (string text in ocrText)
                    {
                        //スキャンした文字列をコンソールに出力
                        Console.WriteLine(ocrText);
                    }
                    //スキャンした文字列をサブフォルダに書き出す
                    string logPath = Directory.GetCurrentDirectory() + @"\Debug\scan_mvp.log";
                    //logPath.Replace(@"\\",@"\");
                    if (!File.Exists(logPath))
                        File.Create(logPath);
                    StreamWriter writer = new StreamWriter(logPath, true, Encoding.GetEncoding("Shift_JIS"));
                    writer.WriteLine(DateTime.Now.ToString() + "\n");
                    writer.WriteLine("ウィンドウ名：" + _targetWindowTitle + "\n");

                    writer.WriteLine("------MVPスキャン-------\n");
                    foreach (string text in ocrText)
                    {
                        writer.WriteLine(text);
                    }
                    writer.WriteLine("------MVPスキャンここまで-------\n");
                    writer.Close();
                    writer.Dispose();

                    //取得した画像をサブフォルダに書き出す
                    chatWindowImage.Save(@"Debug\" + "CaptureImage_MVP.bmp", ImageFormat.Bmp);
                }
                //=====デバッグ用=====

                //スキャンした文字からMVP検知対象のキーワードをチェック
                for(int i = ocrText.Count-1;i > 0; i--)
                {
                    if (CheckKeywordsMVP(ocrText[i]) && GetStringDiffRate(ocrText[i],_prev_mvp) < TEXT_MATCH_RATIO)
                    {
                        if ((DateTime.Now - _timeCache).TotalMinutes < (double)_mvp_idleTime)
                        {
                            _speech.Speak("MVP検知　");
                            //_speech.Speak(ocrText[i]);
                            _prev_mvp = ocrText[i];
                            _timeCache = DateTime.Now;
                        }
                        break;
                    }
                }

                ocrText.Clear();
                cvBitmap.Dispose();
                ocrResult.Dispose();
            }


            //▼▼探知機検出処理▼▼
            //グレースケール化
            if (_isGrayScale)
                captureImage = GrayScale(captureImage);

            if (_isSplit)
            {
                //分割スキャンモード

                //キャプチャした画像を分割
                List<Bitmap> splitCaptureImage = SplitCaptureImage(captureImage);

                //分割した画像をそれぞれOCRスキャン
                foreach (Bitmap image in splitCaptureImage)
                {
                    //キャプチャした画像をOCR用に変換
                    Task<SoftwareBitmap> cvBitmap = GetSoftwareSnapShot(image);
                    cvBitmap.Wait();
                    SoftwareBitmap ocrImage = cvBitmap.Result;

                    //OCRスキャン
                    Task<OcrResult> ocrResult = RecognizeText(ocrImage);
                    ocrResult.Wait();

                    //スキャン結果を行ごとに処理
                    foreach (var line in ocrResult.Result.Lines)
                    {
                        //スキャンした文字列を変数に追加
                        ocrText.Add(line.Text);
                    }

                    cvBitmap.Dispose();
                    ocrResult.Dispose();

                }
                //=====デバッグ用=====
                if (_isDebug)
                {
                    foreach(string text in ocrText)
                    {
                        //スキャンした文字列をコンソールに出力
                        Console.WriteLine(ocrText);
                    }

                    //取得した画像をサブフォルダに書き出す
                    for (int i = 0; i < splitCaptureImage.Count; i++)
                    {
                        splitCaptureImage[i].Save(@"Debug\" + "CaptureImage_SplitMode_" + i + ".bmp", ImageFormat.Bmp);
                    }
                }
                //=====デバッグ用=====
                splitCaptureImage.Clear();
            }
            else
            {
                //通常スキャンモード
                //キャプチャした画像をOCR用に変換
                Task<SoftwareBitmap> cvBitmap = GetSoftwareSnapShot(captureImage);
                cvBitmap.Wait();
                SoftwareBitmap ocrImage = cvBitmap.Result;


                //OCRスキャン
                Task<OcrResult> ocrResult = RecognizeText(ocrImage);
                ocrResult.Wait();

                //スキャン結果を行ごとに処理
                foreach(var line in ocrResult.Result.Lines)
                {
                    //スキャンした文字列を変数に追加
                    ocrText.Add(line.Text);
                }

                //=====デバッグ用=====
                if (_isDebug)
                {
                    foreach (string text in ocrText)
                    {
                        //スキャンした文字列をコンソールに出力
                        Console.WriteLine(ocrText);
                    }

                    //取得した画像をサブフォルダに書き出す
                    captureImage.Save(@"Debug\" + "CaptureImage_NormalMode.bmp", ImageFormat.Bmp);
                }
                //=====デバッグ用=====

                cvBitmap.Dispose();
                ocrResult.Dispose();
            }


            //=====デバッグ用=====
            if (_isDebug)
            {
                //スキャンした文字列をサブフォルダに書き出す
                string logPath = Directory.GetCurrentDirectory() + @"\Debug\scan.log";
                //logPath.Replace(@"\\",@"\");
                if (!File.Exists(logPath))
                    File.Create(logPath);
                StreamWriter writer = new StreamWriter(logPath, true, Encoding.GetEncoding("Shift_JIS"));
                writer.WriteLine(DateTime.Now.ToString() + "\n");
                writer.WriteLine("ウィンドウ名：" + _targetWindowTitle + "\n");

                writer.WriteLine("------スキャン-------\n");
                foreach (string text in ocrText)
                {
                    writer.WriteLine(text);
                }
                writer.WriteLine("------スキャンここまで-------\n");
                writer.Close();
                writer.Dispose();
            }
            //=====デバッグ用=====

            //スキャンした文字から検知対象のキーワードをチェック
            bool isDetector = false;
            
            //行ごとに処理
            foreach (string line in ocrText)
            {
                if (CheckKeywords(line) && !CheckIgnoreKeywords(line))
                    isDetector = true;
            }

            if (isDetector)
            {
                var icon = GetResourceStream(new Uri("./Resources/detector_r.ico", UriKind.Relative)).Stream;
                _notifyIcon.Icon = new System.Drawing.Icon(icon);
                _sound.PlayLooping();

            }
            else
            {
                var icon = GetResourceStream(new Uri("./Resources/detector_g.ico", UriKind.Relative)).Stream;
                _notifyIcon.Icon = new System.Drawing.Icon(icon);
                _sound.Stop();
            }
            captureImage.Dispose();
        }

        //=============================================================
        //指定したウィンドウタイトルのハンドルを取得
        //=============================================================
        private IntPtr GetWindowHandle(string windowTitle)
        {
            IntPtr hWnd = IntPtr.Zero;
            try
            {
                foreach (Process pList in Process.GetProcesses())
                {
                    if (pList.MainWindowTitle.Contains(windowTitle))
                    {
                        hWnd = pList.MainWindowHandle;
                        Console.WriteLine(pList.ProcessName);
                    }
                    pList.Dispose();
                }
            }
            catch (Exception e) { Console.WriteLine("GetWindowHandle:" + e.Message.ToString()); }
            return hWnd;
        }

        //=============================================================
        //指定ウィンドウのキャプチャ
        //=============================================================
        private Bitmap CaptureTargetWindow()
        {
            /*
             * ウィンドウキャプチャ：パターン①(指定ウィンドウが裏にあっても撮れる？)
             */

            Bitmap bitmap = null;
            try
            {
                //ウィンドウハンドルを取得
                IntPtr handle = GetWindowHandle(_targetWindowTitle);

                //ウィンドウサイズ取得
                RECT rect;
                bool flag = GetWindowRect(handle, out rect);

                int width = rect.right - rect.left > 0 ? rect.right - rect.left : 1;
                int height = rect.bottom - rect.top > 0 ? rect.bottom - rect.top : 1;

                IntPtr hWndDc = GetDC(handle);
                IntPtr hMemDc = CreateCompatibleDC(hWndDc);
                IntPtr hBitmap = CreateCompatibleBitmap(hWndDc, width, height);
                SelectObject(hMemDc, hBitmap);

                BitBlt(hMemDc, 0, 0, width, height, hWndDc, 0, 0, TernaryRasterOperations.SRCCOPY);
                bitmap = Bitmap.FromHbitmap(hBitmap);

                DeleteObject(hBitmap);
                ReleaseDC(handle, hWndDc);
                ReleaseDC(IntPtr.Zero, hMemDc);
                DeleteObject(hMemDc);
                DeleteObject(hWndDc);
            }
            catch (Exception e) { Console.WriteLine("CaptureTargetWindow:" + e.Message.ToString()); }

            return bitmap;
        }

        //=============================================================
        //キャプチャした画像を分割する(精度アップ用)
        //=============================================================
        private List<Bitmap> SplitCaptureImage(Bitmap captureImage)
        {
            const int splitX = 4;   //横の分割数
            const int splitY = 3;   //縦の分割数

            List<Bitmap> splitImage = new List<Bitmap>();
            Rectangle splitRect;
            int c_width = captureImage.Width;
            int c_height = captureImage.Height;

            int s_width = c_width / splitX;
            int s_height = c_height / splitY;

            int left, top;

            //キャプチャした画像を分割してリストに格納
            //横
            for (int i = 0; i < splitX; i++)
            {
                //縦
                for (int j = 0; j < splitY; j++)
                {
                    left = s_width * i;
                    top = s_height * j;

                    splitRect = new Rectangle(left, top, s_width, s_height);
                    splitImage.Add(captureImage.Clone(splitRect, captureImage.PixelFormat));
                }
            }
            return splitImage;
        }

        //=============================================================
        //キャプチャした画像を指定の領域でトリミングする
        //=============================================================
        private Bitmap TrimCaptureImage(Bitmap captureImage,int ltx,int lty,int rbx,int rby) 
        {

            int trimWidth = rbx - ltx;
            int trimHeight = rby - lty;

            if (captureImage.Size.Width < ltx + trimWidth)
                trimWidth = captureImage.Size.Width - ltx;
            if (captureImage.Size.Height < lty + trimHeight)
                trimHeight = captureImage.Size.Height - lty;

            if (trimWidth <= 0 || trimHeight <= 0)
                return captureImage;

            Rectangle trimRect = new Rectangle(ltx, lty, trimWidth, trimHeight);
            captureImage = captureImage.Clone(trimRect, captureImage.PixelFormat);

            return captureImage;
        }

        //=============================================================
        //キャプチャした画像をグレースケールに変換する(精度アップ用)
        //=============================================================
        private Bitmap GrayScale(Bitmap captureImage)
        {
            int width = captureImage.Width;
            int height = captureImage.Height;

            Bitmap grayBmp = new Bitmap(width, height);         // グレースケールの画像。

            // 全ピクセルを1つずつグレースケールに変換する。
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    try
                    {
                        // NTSC規格などに準拠せず、RGB値の平均値をグレースケールに変える。
                        Color pixelColor = captureImage.GetPixel(i, j);
                        byte grayScale = Convert.ToByte((pixelColor.R + pixelColor.G + pixelColor.B) / 3);
                        // RGBだけでなく、アルファ値もセットする。
                        Color grayColor = Color.FromArgb(pixelColor.A, grayScale, grayScale, grayScale);
                        grayBmp.SetPixel(i, j, grayColor);
                    }
                    catch
                    {
                        System.Console.Error.WriteLine("(" + i + "," + j + ")ピクセルでエラーが発生しました。");
                    }
                }
            }
            return grayBmp;
        }

        //=============================================================
        //キャプチャした画像をOCRスキャン用の形式に変換する
        //=============================================================
        private async Task<SoftwareBitmap> GetSoftwareSnapShot(Bitmap snap)
        {
            SoftwareBitmap sbmp = null;

            try
            {
                // 取得したキャプチャ画像をファイルとして保存
                var folder = Directory.GetCurrentDirectory();
                var imageName = "ScreenCapture.bmp";
                StorageFolder appFolder = await StorageFolder.GetFolderFromPathAsync(@folder);
                snap.Save(folder + "\\" + imageName, ImageFormat.Bmp);
                SoftwareBitmap softwareBitmap;
                var bmpFile = await appFolder.GetFileAsync(imageName);

                // 保存した画像をSoftwareBitmap形式で読み込み
                using (IRandomAccessStream stream = await bmpFile.OpenAsync(FileAccessMode.Read))
                {
                    BitmapDecoder decoder = await BitmapDecoder.CreateAsync(stream);
                    softwareBitmap = await decoder.GetSoftwareBitmapAsync();
                }

                // 保存した画像ファイルの削除(デバッグ時はコメントアウトすると良い)
                File.Delete(folder + "\\" + imageName);

                // SoftwareBitmap形式の画像を返す
                sbmp = softwareBitmap;
            }
            catch (Exception e) { Console.WriteLine("GetSoftwareSnapShot:" + e.Message.ToString()); }
            return sbmp;
        }

        //=============================================================
        //OCRスキャン
        //=============================================================
        private async Task<OcrResult> RecognizeText(SoftwareBitmap snap)
        {
            OcrEngine ocrEngine = OcrEngine.TryCreateFromUserProfileLanguages();
            // OCR実行
            var ocrResult = await ocrEngine.RecognizeAsync(snap);
            return ocrResult;
        }

        //=============================================================
        //文字列から検知対象のキーワードをチェックする
        //=============================================================
        private bool CheckKeywords(string scanData)
        {
                scanData = scanData.Replace(" ", "");
                foreach (string keyword in _keywords)
                {
                    
                    if (scanData.IndexOf(keyword) != -1)
                        return true;
                }
            return false;
        }

        //=============================================================
        //文字列から除外対象のキーワードをチェックする
        //=============================================================
        private bool CheckIgnoreKeywords(string scanData)
        {
                scanData = scanData.Replace(" ", "");
                foreach (string keyword in _keywords_ignore)
                {
                    if (scanData.IndexOf(keyword) != -1)
                        return true;
                }          
            return false;
        }

        //=============================================================
        //文字列からMVP検知対象のキーワードをチェックする
        //=============================================================
        private bool CheckKeywordsMVP(string scanData)
        {
            scanData = scanData.Replace(" ", "");
            foreach (string keyword in _keywords_mvp)
            {
                if (scanData.IndexOf(keyword) != -1)
                    return true;
            }
            return false;
        }

        //=============================================================
        //文字列同士の一致度を計算する
        //=============================================================
        private double GetStringDiffRate(string str1, string str2)
        {
            var levenshteinDistance = Levenshtein.Distance(str1, str2);
            var length = (str1.Length > str2.Length) ? str1.Length : str2.Length;
            var diffRatio = 1.0 - ((double)levenshteinDistance / length);
            return diffRatio;
        }


        //####################################################################################################
        //▼▼特にいじる必要の無いもの▼▼
        //####################################################################################################

        private enum TernaryRasterOperations : uint
        {
            /// <summary>dest = source</summary>
            SRCCOPY = 0x00CC0020,
            /// <summary>dest = source OR dest</summary>
            SRCPAINT = 0x00EE0086,
            /// <summary>dest = source AND dest</summary>
            SRCAND = 0x008800C6,
            /// <summary>dest = source XOR dest</summary>
            SRCINVERT = 0x00660046,
            /// <summary>dest = source AND (NOT dest)</summary>
            SRCERASE = 0x00440328,
            /// <summary>dest = (NOT source)</summary>
            NOTSRCCOPY = 0x00330008,
            /// <summary>dest = (NOT src) AND (NOT dest)</summary>
            NOTSRCERASE = 0x001100A6,
            /// <summary>dest = (source AND pattern)</summary>
            MERGECOPY = 0x00C000CA,
            /// <summary>dest = (NOT source) OR dest</summary>
            MERGEPAINT = 0x00BB0226,
            /// <summary>dest = pattern</summary>
            PATCOPY = 0x00F00021,
            /// <summary>dest = DPSnoo</summary>
            PATPAINT = 0x00FB0A09,
            /// <summary>dest = pattern XOR dest</summary>
            PATINVERT = 0x005A0049,
            /// <summary>dest = (NOT dest)</summary>
            DSTINVERT = 0x00550009,
            /// <summary>dest = BLACK</summary>
            BLACKNESS = 0x00000042,
            /// <summary>dest = WHITE</summary>
            WHITENESS = 0x00FF0062
        }

        [DllImport("gdi32.dll", ExactSpelling = true, PreserveSig = true, SetLastError = true)]
        static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

        [DllImport("gdi32.dll")]
        private static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

        [DllImport("gdi32.dll", SetLastError = true)]
        private static extern IntPtr CreateCompatibleDC(IntPtr hdc);

        [DllImport("gdi32.dll")]
        private static extern bool DeleteObject(IntPtr hObject);

        [DllImport("gdi32.dll")]
        private static extern IntPtr CreateBitmap(int nWidth, int nHeight, uint cPlanes, uint cBitsPerPel, IntPtr lpvBits);

        [DllImport("user32.dll")]
        private static extern IntPtr GetDC(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern int ReleaseDC(IntPtr hWnd, IntPtr hDC);

        [DllImport("gdi32.dll")]
        private static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, TernaryRasterOperations dwRop);

    }
}
